function getfile(filename) {
    var promiseObj = new Promise(function (success, fail) {
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (xhttp.readyState === 4 && xhttp.status === 200) {
                success(xhttp.responseText);
            }
            if (xhttp.readyState === 4 && xhttp.status !== 200) {
                fail(xhttp.status);
            }
        };
        xhttp.open("GET", filename);
        xhttp.send();
    });
    return promiseObj;
}

var lang;

function getlang() {
    return getfile("lang.txt").then(function (str) {
        lang = str;
    }, console.log);
}
//assumes only one language available

var gametiles_raw;
var gametiles = [];
var keyboard_raw;
var keyboard;
var wordlist_raw;
var wordlist;
var lop;
var lwc;

function parseGametiles(str) {
    gametiles_raw = str.trim();
    var rows = gametiles_raw.split("\n");
    var i;
    for (i = 1; i < rows.length; i += 1) {
        gametiles.push([]);
        gametiles[i - 1] = {
            tiles: rows[i].split("\t")[0],
            or1: rows[i].split("\t")[1],
            or2: rows[i].split("\t")[2],
            or3: rows[i].split("\t")[3]
        };
    }
}

function getGametiles() {
    gametiles = [];
    return getfile(lang.trim() + "/text/aa_gametiles.txt").then(parseGametiles);
} //function in case one wants to change language.

function getKeyboard() {
    return getfile(lang.trim() + "/text/aa_keyboard.txt").then(function (str) {
        keyboard_raw = str.trim();
        keyboard = keyboard_raw.split("\n");
    });
}

function parseWordlist(str) {
    wordlist_raw = str.trim();
    var rows;
    var i;
    rows = wordlist_raw.split("\n");
    lwc = rows[0].split("\t")[0];
    lop = rows[0].split("\t")[1];
    wordlist = [];
    for (i = 1; i < rows.length; i += 1) {
        wordlist.push([]);
        wordlist[i - 1] = {
            lwc: rows[i].split("\t")[0],
            lop: rows[i].split("\t")[1]
        };
    }
    return wordlist;
}

function getWordlist() {
    return getfile(lang.trim() + "/text/aa_wordlist.txt").then(parseWordlist);
}

function grabImageForWord(num) {
    var ret = document.createElement("img");
    ret.src = lang.trim() + "/images/" + wordlist[num].lwc + ".jpg";
    ret.classList.add("wordimage");
    return ret;
}
function grabAudioForWord(num) {
    var ret = new Audio(lang.trim() + "/audio/" + wordlist[num].lwc + ".mp3");
    return ret;
}
