function randomInclusive(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min + 1)) + min; //The maximum is inclusive and the minimum is inclusive 
}

/**
 * Shuffles array in place.
 * @param {Array} a items An array containing the items.
 */
function shuffle(a) {
    var j, x, i;
    for (i = a.length - 1; i > 0; i--) {
        j = Math.floor(Math.random() * (i + 1));
        x = a[i];
        a[i] = a[j];
        a[j] = x;
    }
    return a;
}

var regex=null
function tokens(word){
	if(null === regex){
		var mytiles = [];
		for(var i = 0; i < gametiles.length; i+=1){
			mytiles.push(gametiles[i].tiles);
		}
		mytiles.sort(function(a,b){
			return b.length-a.length;
		});
		//console.log(mytiles);
		regex=new RegExp("("+mytiles.join("|")+")","gi")
	}
	return word.match(regex);
}

var user=localStorage.getItem("user");
if(null===user && window.location.href.toLowerCase().match(/.*\/user\.html/i)===null){
	window.location.replace("user.html");
}
function storeScore(game, score){
	localStorage.setItem(user+"."+game+".score",score);
}

function storeOther(game,name,value){
	localStorage.setItem(user+"."+game+"."+name,value);
}

function getScore(game){
	return localStorage.getItem(user+"."+game+".score");
}

function getOther(game,name){
	return localStorage.getItem(user+"."+game+"."+name);
}
