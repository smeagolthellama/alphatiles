function load_fragments() {
	'use strict';
	var list, i, xhttp;
	list = document.getElementsByClassName("fragment");
	for (i = 0; i < list.length; i += 1) {
		list[i].addEventListener("prepared", 
		function(event){
			'use strict';
			var script, xhttp = new XMLHttpRequest();
			xhttp.onreadystatechange = function () {
				if (this.readyState === 4 && this.status === 200) {
					event.target.innerHTML = this.responseText;
					//console.log(this);
				//	console.log(event.target);
					script = document.createElement("script");
					script.src = "fragments/" + event.target.dataset.fragment + ".js";
					document.head.appendChild(script);
				}
			};
			xhttp.open("GET", "fragments/" + event.target.dataset.fragment + ".html", true);
			xhttp.send();
		});
		list[i].dispatchEvent(new Event("prepared"));
	}
}

window.onload = load_fragments;
